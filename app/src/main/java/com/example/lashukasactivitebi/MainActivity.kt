package com.example.lashukasactivitebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    private lateinit var nametext1 : EditText
    private lateinit var button1: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nametext1 = findViewById(R.id.textname1)
        button1 = findViewById(R.id.button1)

        button1.setOnClickListener{
            val name = nametext1.text.toString()

            val intent = Intent(this,lashukasactivity2::class.java)

            intent.putExtra("name",name)

            startActivity(intent)
        }
    }
}