package com.example.lashukasactivitebi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class lashukasactivity2 : AppCompatActivity() {

    private lateinit var age : EditText
    private lateinit var button2: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lashukasactivity2)

        age = findViewById(R.id.textname2)
        button2 = findViewById(R.id.button2)

        var name = ""

        if (intent.extras != null) {

            name = intent.extras?.getString("name","").toString()


        }

        button2.setOnClickListener {
            val age = age.text.toString().toInt()

            val intent = Intent(this,lashukasactivitebi3::class.java)

            intent.putExtra("age", age)

            intent.putExtra("name", name )

            startActivity(intent)


        }
    }
}